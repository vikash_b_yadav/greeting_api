using ApiExample.ApplicationCore.Greetings;
using NUnit.Framework;

namespace ApiExample.ApplicationCore.UnitTests.Greetings
{
    public class GreetingServiceTests
    {
        private GreetingService sut;

        [SetUp]
        public void Setup()
        {
            sut = new GreetingService();
        }

        [Test]
        public void GetMessage_ShouldReturnHelloWorldMessage()
        {
            var expectedMessage = "Welcome to BBC Studios";

            var result = sut.GetMessage();

            Assert.AreEqual(expectedMessage, result);
        }
    }
}