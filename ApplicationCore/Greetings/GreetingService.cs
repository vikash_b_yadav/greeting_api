﻿using ApiExample.ApplicationCore.Services;

namespace ApiExample.ApplicationCore.Greetings
{
    public class GreetingService : IGreetingService
    {
        public string GetMessage()
        {
            return "Welcome to BBC Studios";
        }
    }
}