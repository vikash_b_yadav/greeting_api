﻿namespace ApiExample.ApplicationCore.Services
{
    public interface IGreetingService
    {
        string GetMessage();
    }
}