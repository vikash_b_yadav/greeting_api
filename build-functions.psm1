<# Note: Requires Powershell 2 or above

# What does this script do? Provides functions that do the following

	* Cleans the directory ..\dist\ for your build output to be placed into
	* Restores nuget dependencies with the Restore command
	* Compiles with msbuild the sln file in the same directory as this script using the configured version of msbuild and sensible defaults
	* OctoPacks any projects with octopack installed included to the release directory
	* Nuget packs any projects containing a nuspec file
	* Executes any NUnit tests identified based on naming convention
	* Logs build and tests outputs to files for you
	* Throws errors if Build() or Test() fail
	* Reports progress to the console and teamcity nicely

# How to use this library to create your own build script?

	1. Place this script in same folder as your solutions .sln file
	2. Install-Package NUnit.ConsoleRunner in your tests project
	3. If you are using octopus deploy then Install-Package OctoPack in each project to be deployed (db, website etc)
	3. Copy the following into a new file build.ps1 and place that file alongside this file
	*************************************** build.ps1 ************************************************
		Import-Module "$PSScriptRoot\build-functions.psm1" -Force

		# Execute the build!

		$config = "Release"
		Clean -Config $config

		Restore
		# OR for dotnet core change to the line below
		# Restore -UseDotNet
		
		Invoke-Build -Config $config
		# OR for dotnet core change to the line below
		# Invoke-BuildCore -Config $config

		Invoke-TestNunit -Assembly "*UnitTests.dll" -NUnitConsole "nunit3-console.exe" -Config $config
		# OR for dotnet core change to the line below
		# Invoke-TestCore -ProjectNameFilter "*UnitTest*" -Config $config
		
		Pack -Config $config

		ReportResults

	***************************************    Ends    ************************************************
	4. Execute the script with powershell PS> .\build.ps1

 #> 

# Configurable variables default values (override in your build script):
	$OutputDirectory     = "..\dist"
    $InfoMessageColour   = "Magenta"

# Build and test functions:

	# pretty print messages
	Function InfoMessage ([string] $message)
	{ 
		Write-Host "$message`n" -ForegroundColor $infoMessageColour
	}

	Function WarnMessage ([string] $message)
	{ 
		Write-Host "Warning: $message`n" -ForegroundColor "Yellow"
	}		
	
	# finds the most recent file below this directory matching a pattern
	Function GetMostRecentFileMatchingPath([string] $FilePathPattern, [switch] $IgnoreError) #e.g GetMostRecentFileMatchingPath("*.sln")
	{
		$file = Get-ChildItem -Path $PSScriptRoot -Recurse -Filter $filePathPattern | Sort LastWriteTime | Select -last 1
		if($file -eq "" -or $file -eq $null){
			if(!$IgnoreError){
			throw "Unable to find a file in $SolutionFolder (or below) matching $filePathPattern"
			}
			return $null;
		}
		return $file
	}

	# find all the unique folders that contain a file matching a specification
	Function GetFoldersContainingFilesThatMatch([string] $FilePattern, [string] $ExcludePattern)
	{
		$items = Get-ChildItem -Filter $FilePattern -Recurse `
			| Select-Object -expandproperty FullName `
			| Get-Unique `
			| Where { $_ -NotMatch $ExcludePattern } `
			| foreach { Split-Path $_ -Parent } 

		return $items
	}

	

	 Function Invoke-Clean([string] $MSBuild = "(auto)", [string] $Config = "Release") 
	 {
		 if($MSBuild -eq "(auto)")
		 {
			 $MSBuild = Find-MsBuild
		 }		 
		 
		 InfoMessage "Clean step: Emptying $ReleaseDir"
		 $removed = Remove-Item -path $ReleaseDir -recurse -force -ErrorAction silentlycontinue
		 $new = New-Item -path $ReleaseDir -type directory -force -ErrorAction silentlycontinue
		 $ReleaseDir = Resolve-Path $ReleaseDir
 
		 $MsBuildCleanArgs = $PathToSln, "/t:clean", "/m", "/p:Configuration=$Config", "/noconsolelogger"
		 InfoMessage "MsBuild Clean: `n  $MsBuild $MsBuildCleanArgs"
		 & $MsBuild $MsBuildCleanArgs
	 }
	
	# Try and find a recent version of msbuild
	Function Find-MsBuild([int] $MaxVersion = 2019)
	{
		$agent2019Path = "$Env:programfiles\Microsoft Visual Studio\2019\BuildTools\MSBuild\Current\Bin\msbuild.exe"
		$agentPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin\msbuild.exe"
		$devPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\msbuild.exe"
		$proPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\msbuild.exe"
		$communityPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\msbuild.exe"
		$fallback2015Path = "${Env:ProgramFiles(x86)}\MSBuild\14.0\Bin\MSBuild.exe"
		$fallback2013Path = "${Env:ProgramFiles(x86)}\MSBuild\12.0\Bin\MSBuild.exe"
		$fallbackPath = "C:\Windows\Microsoft.NET\Framework\v4.0.30319"
		
		$devPath2019 = "$Env:programfiles (x86)\Microsoft Visual Studio\2019\Enterprise\MSBuild\Current\Bin\msbuild.exe"
	    $proPath2019 = "$Env:programfiles (x86)\Microsoft Visual Studio\2019\Professional\MSBuild\Current\Bin\msbuild.exe"
		
		If ((2019 -le $MaxVersion) -And (Test-Path $agent2019Path)) { return $agent2019Path } 
		If ((2019 -le $MaxVersion) -And (Test-Path $devPath2019)) { return $devPath2019 } 
		If ((2019 -le $MaxVersion) -And (Test-Path $proPath2019)) { return $proPath2019 }
		If ((2017 -le $MaxVersion) -And (Test-Path $agentPath)) { return $agentPath } 
		If ((2017 -le $MaxVersion) -And (Test-Path $devPath)) { return $devPath } 
		If ((2017 -le $MaxVersion) -And (Test-Path $proPath)) { return $proPath } 
		If ((2017 -le $MaxVersion) -And (Test-Path $communityPath)) { return $communityPath } 
		If ((2015 -le $MaxVersion) -And (Test-Path $fallback2015Path)) { return $fallback2015Path } 
		If ((2013 -le $MaxVersion) -And (Test-Path $fallback2013Path)) { return $fallback2013Path } 
		If (Test-Path $fallbackPath) { return $fallbackPath } 
		
		throw "Yikes - Unable to find msbuild"
	}

	# Return a path to nuget.exe. Tries the following locations in order:
	# - find it in child folders such as Nuget.CommandLine/octopack packages folders
	# - see if it is available on the path
	# - try and download it from nuget.org to the packages folder 
	# - throw error if it cannot be found
	Function Find-Nuget([string] $Executable = "nuget")
	{
		$localNugetPath = (GetMostRecentFileMatchingPath $Executable -IgnoreError)
		$NugetExePath = If($localNugetPath -ne $null) { $localNugetPath.FullName } Else {"nuget"}

		# cant find it locally or path? then download
		if ((Get-Command $NugetExePath -ErrorAction SilentlyContinue) -eq $null) 
		{ 
			$sourceNugetExe = "https://dist.nuget.org/win-x86-commandline/latest/nuget.exe"
			$NugetExePath = "$SolutionFolder\packages\nuget.exe"

			WarnMessage "Unable to find nuget.exe in your PATH or in your project. Trying to get it from the web! Downloading $sourceNugetExe to $NugetExePath"

			mkdir "packages" -Force | Out-Null
			$result = Invoke-WebRequest $sourceNugetExe -OutFile $NugetExePath -PassThru -UseBasicParsing

			if($result.StatusCode -ne 200) { 
				throw "Unable to download nuget from the web so Run 'Install-Package Nuget.CommandLine' to fix"
			} else {
				InfoMessage "Downloaded nuget to $NugetExePath"
			}
		}

		return $NugetExePath
	}

	Function Restore([switch] $UseDotnet)
	{
		if($UseDotnet) {
			$NugetExePath = "dotnet"
		} else {
			$NugetExePath = Find-Nuget
		}

		$NugetArgs =  "restore", $SolutionFolder
					
		InfoMessage "Restore nuget packages: `n  $NugetExePath $NugetArgs" 
		& $NugetExePath $NugetArgs
	}

	# use msbuild to compile the sln file
    # to find msbuild in 2017+ consider using vswhere, see https://github.com/Microsoft/vswhere/wiki/Find-MSBuild
	# or use %programfiles(x86)%\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin on agents and C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin on dev
	Function Build([string] $Config = "Release", [string] $MSBuild = "(auto)", [string] $MsBuildArgs ="") 
	{
		WarnMessage "'Build' function is deprecated and will be removed - use 'Invoke-Build' or 'Invoke-BuildCore' instead"

		Invoke-Build -Config $config -MsBuild $MsBuild -MsBuildArgs $MsBuildArgs
	}	

	Function Invoke-Build([string] $MSBuild = "(auto)", [string] $MsBuildArgs ="", [string] $Config = "Release") 
	{
		if($MSBuild -eq "(auto)"){
			$MSBuild = Find-MsBuild
		}

		InfoMessage "Build: Compiling $PathToSln in $Config configuration"
		InfoMessage "Log file: $LogFile"

        $UseOctopack = $false
        If((GetMostRecentFileMatchingPath "OctoPack.Tasks.dll" -IgnoreError) -ne $null){
            InfoMessage "Detected you have Octopack installed - Adding RunOctoPack=true to MsBuild parameters for automatic packing"
            $UseOctopack = $true
        }

        $OctopackMsbuildParams = If($UseOctopack) { "/p:RunOctoPack=true;OctoPackPublishPackageToFileShare=$ReleaseDir;OctoPackPublishPackagesToTeamCity=false" } else { "" }
    
		$FinalMsBuildArgs = $MsBuildArgs, $PathToSln,  $OctopackMsbuildParams, "/t:build", "/p:Configuration=$config","/noautorsp", "/ds", "/m", "/l:FileLogger,Microsoft.Build.Engine;logfile=$LogFile"
        write-host($MsBuildArgs)

		InfoMessage "Executing MsBuild: `n  $MSBuild $FinalMsBuildArgs"

		& $MsBuild $FinalMsBuildArgs
	}

	Function Invoke-BuildCore([string] $BuildCommand ="dotnet", [string] $BuildArguments ="build", [string] $Config = "Release") 
	{
		InfoMessage "Using $BuildCommand $BuildArguments to compile $PathToSln in $Config configuration"
		InfoMessage "Log file: $LogFile"

		$FinalMsBuildArgs = $BuildArguments, $PathToSln, "--no-restore", "--configuration", $Config, "/flp:logfile=$LogFile"

		InfoMessage "Executing Build: `n  $BuildCommand $FinalMsBuildArgs"

		& $BuildCommand $FinalMsBuildArgs
	}

	# execute any nunit tests identified by tests naming convention 
	Function Test-NUnit([string] $Assembly = "*Tests.dll", [string] $NUnitConsole = "nunit3-console.exe", [string] $Config = "Release") 
	{
		WarnMessage "'Test-NUnit' function is deprecated and will be removed - use 'Invoke-TestNunit' for nunit3-console or 'Invoke-TestCore' for dotnet test instead"
		Invoke-TestNUnit -Assembly $Assembly -NUnitConsole $NUnitConsole -Config $Config
	}		

	# Run NUnit tests using the nunit console test runner. Finds all tests DLLs matching a pattern and executes the nunit tests
	# Will report results to Teamcity (if available) and outputs the test result file to the Release directory
	# TODO: each call should produce different test file outpus ($NunitTestOutput) but at the moment they overwrite each other
	Function Invoke-TestNUnit([string] $Assembly = "*Tests.dll", [string] $NUnitConsole = "nunit3-console.exe", [string] $Config = "Release") 
	{
		if($LASTEXITCODE -eq 0){
			$NUnitConsolePath = (GetMostRecentFileMatchingPath $NUnitConsole -IgnoreError).FullName

			if($NUnitConsolePath) {
				InfoMessage "NUnit tests: Finding tests matching $Assembly in bin folders."
		
				# Find tests in child folders (except obj)
				$TestDlls = Get-ChildItem -Path $PSScriptRoot -Recurse  -Include $Assembly | Select-Object -expandproperty FullName | Where-Object {$_ -NotLike "*\obj\*"} | % { "`"$_`"" }

				if(@($TestDlls).Count -eq 0)  {
					# Add the tests in the output folders (except obj)
					$TestDlls += Get-ChildItem -Path $ReleaseDir -Recurse -Include $Assembly | Select-Object -expandproperty FullName | Where-Object {$_ -NotLike "*\obj\*"} | % { "`"$_`"" }
				}

				$teamcityOption = If($IsTeamcity) { "--teamcity" } Else { "" }

				$NUnitArgs  =  ("/config:$Config", "/process:Multiple", "--result=TestResult.xml", $teamcityOption)
				$NUnitArgs += $TestDlls

				InfoMessage "Found $(@($TestDlls).Count) test DLL(s): $TestDlls. Test Output will save to $nunitTestOutput)"
				InfoMessage "Executing Nunit: `n  $NUnitConsolePath $NUnitArgs     " 

				if(@($TestDlls).Count -eq 0) 
				{
					WarnMessage "No tests found!"
				} else {
					& $NUnitConsolePath $nunitArgs 2>&1 # redirect stderr to stdout, otherwise a build with muted tests is reported as failed because of the stdout text
					
					InfoMessage "Placing test result file at $NunitTestOutput"
					Move-Item -Path "TestResult.xml" -Destination $NunitTestOutput
				}
			} else {
				InfoMessage "Skipping Test-NUnit() - no nunit console available"
			}
		}
		else
		{
			InfoMessage "Skipping Test() - Build() probably failed or no nunit console available"
		}
	}

	# Execute `dotnet test` on each test project matching a pattern such as *UnitTest*
	# for each test project you need to have the following
	# $PS> dotnet add package Microsoft.NET.Test.Sdk
	# $PS> dotnet add package NUnit   
	# $PS> dotnet add package NUnit3TestAdapter
	# $PS> dotnet add package TeamCity.VSTest.TestAdapter
	Function Invoke-TestCore(
		[string] $TestCommand ="dotnet", 
		[string] $TestArguments ="test", 
		[string] $Config = "Release", 
		[string] $ProjectNameFilter = "*UnitTest*") 
	{
		# consider dotnet vstest (Get-ChildItem -recurse -File *.Tests.*dll | ? { $_.FullName -notmatch "\\obj\\?" }) for agregated test run

		if($LASTEXITCODE -eq 0){

			if((Test-Path $TestCommand) -Or (Get-Command $TestCommand -ErrorAction SilentlyContinue) -ne $null) {
				
				InfoMessage "Tests: Finding tests matching $ProjectNameFilter"

				$TestFolders = Get-ChildItem -filter $ProjectNameFilter -Directory -Path $PSScriptRoot
				
				InfoMessage "Found $(@($TestFolders).Count) test folders: $TestFolders."

				$TestFolders | ForEach-Object {
					$FinalTestArgs = $TestArguments, $_.FullName, "-c:$Config", "-r:$ReleaseDir", "--no-build", "--no-restore"
					# ,--logger:trx
					#, --logger:TeamCity

					InfoMessage "Executing Test: `n $TestCommand $FinalTestArgs"
					& $TestCommand $FinalTestArgs     
				}
				
				if(@($TestFolders).Count -eq 0) 
				{
					WarnMessage "No tests found!"
				}
			} else {
				InfoMessage "Skipping Test-Core() - could not find $TestCommand"
			}
		}
		else
		{
			InfoMessage "Skipping Test-Core() - Invoke-BuildCore() probably failed or no test command was available"
		}
	}

	# find folders with nuspec files and pack them using nuget
	Function Pack([string] $Executable = "nuget.exe", [string] $Config = "Release")
	{
		if($LASTEXITCODE -eq 0)
		{
			# use nuget.exe from package Nuget.CommandLine/octopack if possible, else if it is on path, use that
			$NugetExePath = Find-Nuget $Executable

			$FoldersWithNuspecs = GetFoldersContainingFilesThatMatch "*.nuspec" "(packages)|(obj)"
			ForEach($projectFolder In $FoldersWithNuspecs)
			{
				# assuming here that we want to ensure the folder has a csproj
				If(Test-Path (Join-Path $projectFolder "*.csproj")){
					$projectToPack = Join-Path $projectFolder "*.csproj" -Resolve
					$NugetArgs =  "Pack", "$projectToPack", "-Properties", "Configuration=$Config", "-OutputDirectory", "$ReleaseDir"
					
					InfoMessage "Executing pack on $projectToPack `n   $NugetExePath $NugetArgs" 
					& $NugetExePath $NugetArgs
				}
			}

			if(@($FoldersWithNuspecs).Count -eq 0) 
			{
				InfoMessage "Skipping Pack() step - no *.nuspec files found to Pack"
			}
		} 
		else
		{
			InfoMessage "Skipping Pack() - previous step failed - probably Test()"
		}
	}

	# raise appropriate failure errors or report sucess
	Function ReportResults()
	{
		$failMessage = ""

		if(Test-Path $LogFile) {
			Get-Content $LogFile -OutVariable BuildSolutionResult | out-null

			if ($BuildSolutionResult.IndexOf("Build succeeded.") -lt 0)
			{
				$failMessage = "MsBuild FAILED! See msbuild log: $LogFile "
			} 
			else{
				InfoMessage "`nMsBuild log file reports success ($LogFile)"
			}
		}

		# Iterate through any .trx test result files in the relase folder and check the outcome
		# Get-ChildItem -Path $ReleaseDir -Filter *.trx | ForEach-Object {
		# 	Get-Content $_.FullName -OutVariable testResult | out-null
		# 	if($testResult -match "<ResultSummary outcome=`"Failed`">" -and $failMessage -eq ""){
		# 		$failMessage = "Tests FAILED! See " + $_.FullName				
		# 	}
		# }

		# check the NUnit tests file for failures
		if(Test-Path $nunitTestOutput) 
		{
			Get-Content $nunitTestOutput -OutVariable testResult | out-null

			if ($testResult -match 'result="Failed"' -and $failMessage -eq "")
			{
				$failMessage = "Tests FAILED! See $nunitTestOutput "
			}
			else{
				InfoMessage "`nNUnit log file reports success ($nunitTestOutput)"
			}
		}

		if($failMessage -ne ""){
			
			throw $failMessage
		}
		else
		{
			$duration = $stopwatch.Elapsed.TotalSeconds
			InfoMessage "`nSuccess! Steps completed successfully in $duration s"
            if($IsTeamcity){ Write-Host "##teamcity[publishArtifacts '$ReleaseDir']" }
		}
	}

<# Computed and other variables #> 
	$PSScriptRoot          = If($PSScriptRoot -eq $null) { Split-Path $MyInvocation.MyCommand.Path -Parent } else { $PSScriptRoot }
	$SolutionFolder        = (Get-Item -Path $PSScriptRoot -Verbose).FullName
	$ProjName              = try {(GetMostRecentFileMatchingPath "*.sln").Name.Replace(".sln", "") } catch {}
	$PathToSln             = "$SolutionFolder\$ProjName.sln"
	$ReleaseDir            = "$SolutionFolder\$OutputDirectory"
	$LogFile               = "$ReleaseDir\$ProjName-Build.log"
	$NunitTestOutput       = "$ReleaseDir\TestResult.xml"
	$Stopwatch             = [Diagnostics.Stopwatch]::StartNew()
	$IsTeamcity            = If(Test-Path env:\TEAMCITY_VERSION) { $true } Else { $false }
	$BuildNumber 		   = if (Test-Path env:\build_number) { $env:build_number } Else { "1.0.0" }

Write-host "Info: Running under Powershell version: " + $PSVersionTable.PSVersion

Export-ModuleMember -Variable OutputDirectory,SolutionFolder,ProjName,PathToSln,ReleaseDir,IsTeamcity,BuildNumber -Function InfoMessage,WarnMessage,GetMostRecentFileMatchingPath,GetFoldersContainingFilesThatMatch,Invoke-Clean,Find-Nuget,Find-MsBuild,Restore,Build,Invoke-Build,Invoke-BuildCore,Test-NUnit,Invoke-TestNUnit,Invoke-TestCore,Pack,ReportResults