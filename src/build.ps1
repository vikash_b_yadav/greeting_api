Import-Module -Name $PSScriptRoot\build-functions.psm1 -Force

$config = "Release"

# Execute the build in stages - Clean > Restore > Compile > Test > Pack > Reporting
Invoke-Clean -Config $config

# Restore Dependencies
Restore 
# OR for dotnet core change to the line below
# Restore -UseDotNet

# Compile the app (and package with octopack if needed)
Invoke-Build -Config $config
# OR for dotnet core change to the line below
# Invoke-BuildCore -Config $config

# Run unit tests
Invoke-TestNunit -Assembly "*UnitTests.dll" -NUnitConsole "nunit3-console.exe" -Config $config
# OR for dotnet core change to the line below
# Invoke-TestCore -ProjectNameFilter "*UnitTest*" -Config $config


# Pack any projects with a nuspec using nuget
#Pack -Config $config

# for dotnet core you need to publish each project
# dotnet publish $PSScriptRoot\MyProject --output "$ReleaseDir/MyProject" --configuration $config

ReportResults