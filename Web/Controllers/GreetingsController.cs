﻿using ApiExample.ApplicationCore.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ApiExample.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GreetingsController : ControllerBase
    {
        private readonly IGreetingService greetingService;
        private readonly ILogger<GreetingsController> logger;

        public GreetingsController(IGreetingService greetingService, ILogger<GreetingsController> logger)
        {
            this.greetingService = greetingService;
            this.logger = logger;
        }

        [HttpGet("{name}")]
        public IActionResult Get(string name="Vikash")
        {
            logger.LogInformation("Getting greeting message.");

            var greetingMessage = greetingService.GetMessage();

            return Ok(new { Message = string.Format("{0} {1}", greetingMessage, name) });
        }
    }
}